---
title: data.table
author: Helen Zhang
date: '2021-04-27'
slug: []
categories: 
  - R
tags:
  - data.table
description: ''
summary: 'data.table notes'
lastmod: "2021-04-29" 
thumbnail: ''
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

101  exercises of data.table   [link](https://www.machinelearningplus.com/data-manipulation/101-r-data-table-exercises/)

#####Q1 How to install data.table and check the version

```{r}

library(data.table)

packageVersion('data.table')

```
#####Q2 Create a pandas series from each of the items below: a list, numpy and a dictionary

```{r}

# input
list_1 <- c("a","b","c","d")
list_2 <- c("1","2","3","4")
list_3 <- c("aa","bb","cc","dd")

DT <- data.table(list_1, list_2, list_3)

head(DT,10)
  
```
#####Q3 Import BostonHousing dataset as a data.table

```{r}

DT <- fread('https://raw.githubusercontent.com/selva86/datasets/master/BostonHousing.csv')

head(DT,5)

```
#####Q4 Import first 50 row of BostonHousing dataset as a data.table

```{r}

DT <- fread('https://raw.githubusercontent.com/selva86/datasets/master/BostonHousing.csv', nrows = 50)

nrow(DT)

```
#####Q5 Import only ‘crim’ and ‘medv’ columns of the BostonHousing dataset as a data.table

```{r}
DT <- fread('https://raw.githubusercontent.com/selva86/datasets/master/BostonHousing.csv', 
            select = c('crim','medv'))

nrow(DT)
ncol(DT)

```
#####Q6 Get the number of rows, columns, datatype and summary statistics of each column of the Cars93 dataset

```{r}

DT <- fread('https://raw.githubusercontent.com/selva86/datasets/master/BostonHousing.csv')

nrow(DT)
ncol(DT)

dim(DT)

sapply(DT, class)

summary(DT)

```
#####Q7 Which manufacturer, model and type has the highest Price? What is the row and column number of the cell with the highest Price value?

```{r}

DT <- fread('https://raw.githubusercontent.com/selva86/datasets/master/Cars93_miss.csv')

#option 1
DT[Price == max(DT$Price, na.rm = TRUE)]

# option 2
DT[order(-Price)][1,.(Manufacturer,Model,Type)]

DT[, row_num := .I][Price == max(DT$Price, na.rm = TRUE),]

DT[, row_num := .I][Price == max(DT$Price, na.rm = TRUE),row_num]


```
#####Q8 Rename the column Model as New_Model in DT

```{r}
DT <- fread('https://raw.githubusercontent.com/selva86/datasets/master/Cars93_miss.csv')

setnames(DT, 'Model', 'New_Model')

head(DT,10)

```
#####Q9 Check if DT has any missing values. Return TRUE is present else FALSE

```{r}

anyNA(DT)

```
#####Q10 Get the location of missing values in DT. Replace the value with TRUE is missing else FALSE

```{r}

head(is.na(DT))

```
#####Q11 Get the row and column positions of missing values in DT

```{r}

# Desired Output
# [1] "(1, 24)"  "(2, 1)"   "(4, 4)"   "(4, 10)"  "(4, 12)"  "(4, 16)"  "(4, 21)" 
#[8] "(5, 4)"   "(5, 6)"   "(5, 9)"   "(6, 10)"  "(6, 25)"  "(7, 6)"   "(7, 15)" 
#[15] "(7, 17)"  "(9, 21)"  "(10, 15)" "(11, 24)" "(12, 9)"  "(12, 10)" "(12, 13)"
#[22] "(13, 11)" "(13, 24)" "(14, 11)" "(14, 17)" "(15, 7)"  "(15, 13)" "(15, 19)"
#[29] "(15, 22)" "(16, 24)" "(17, 13)" "(17, 24)" "(19, 23)" "(19, 24)" "(19, 26)"
#[36] "(20, 1)"  "(20, 22)" "(23, 17)" "(23, 23)" "(24, 15)" "(24, 24)"

```
#####Q12 Count the number of missing values in each column of DT

```{r}

apply(is.na(DT),2,sum)

```
#####Q13 Replace missing values of numeric columns with their respective mean

```{r}
DT <-  fread('https://raw.githubusercontent.com/selva86/datasets/master/Cars93_miss.csv')

head(DT,5)

# get mean for each numeric col
DT[,lapply(.SD,mean,na.rm= TRUE), .SDcols = which(sapply(DT, is.numeric))]

# na impute
DT[,lapply(.SD, function(x) ifelse(is.na(x), mean(x, na.rm = TRUE), x))]

```
#####Q14 Get the column Model in DT as a list
 
```{r}

DT <-  fread('https://raw.githubusercontent.com/selva86/datasets/master/Cars93_miss.csv')

DT[,Model]

```
#####Q15 Get the column Model in DT as a data.table (rather than as a list)

```{r}

head(DT[,"Model"],10)

```
#####Q16 Reorder the rows as per Price column of DT

```{r}

DT[order(Price)]

```
#####Q17 Reorder the rows as per ascending order of Price column and descending order of Max.Price column of DT

```{r}

DT[order(Price, -Max.Price)]

```
#####Q18 Filter out the rows having Manufacturer as Ford and Price less than 30

```{r}

DT[Manufacturer == 'Ford' & Price <30,]

```
#####Q19 Get columns ‘Manufacturer’ & ‘Type’ from data.table and rename them to ‘MANUFACTURER’ & ‘TYPE’

```{r}

setnames(DT, old = c("Manufacturer","Type"), new = c("MANUFACTURER","TYPE"))

head(DT,5)

```
#####Q20 Get mean of all the vehicles having Price greater than 30

```{r}

DT <-  fread('https://raw.githubusercontent.com/selva86/datasets/master/Cars93_miss.csv')

mean(DT[Price >30, Price], na.rm = TRUE)

```
#####Q21  Create new data.table by removing ‘Manufacturer’ & ‘Type’ columns

```{r}
DT2 <- DT[,!c('Manufacturer','Type')]

head(DT2)

```
#####Q22 Calculate number of records for every Manufacturer in DT

```{r}

DT[,.N,by = Manufacturer] 

```
#####Q23 Calculate mean Price for every Manufacturer, Type combination in DT

```{r}

DT[,.(price_avg = mean(Price, na.rm = TRUE)), by = .(Manufacturer, Type)]

```
#####Q24 Calculate mean Price for every Manufacturer, Type combination and arrange it by the keys in DT

```{r}

DT[,.(price_avg = mean(Price, na.rm = TRUE)), keyby = .(Manufacturer, Type)]

```
#####Q25 Select numeric columns from DT

```{r}

DT[,which(sapply(DT, is.numeric)),with = FALSE]


DT[,.SD, .SDcols = which(sapply(DT, is.numeric))]

```
#####Q26 Calculate mean of all numeric columns for every Manufacturer

```{r}

DT[,lapply(.SD, function(x) mean(x, na.rm = TRUE)), .SDcols = which(sapply(DT, is.numeric)), by = Manufacturer]

```
#####Q27 Get first 3 rows for every Manufacturer in DT

```{r}

DT[order(Manufacturer)][, row_id := .I, by = Manufacturer]

DT

```
#####Q28 Remove column Model from DT

```{r}

# DT[,!c('Model')]

DT[, Model := NULL]

DT

```
#####Q29 Set column AirBags as key to the data.table

```{r}

setkey(DT, cols = AirBags)

key(DT)

```
#####Q30 Get all rows where AirBags are Driver & Passenger in DT

```{r}

DT["Driver & Passenger",]

```
#####Q31 Get all rows where AirBags are Driver & Passenger & No data

```{r}
# No data is all NA?

DT[c("Driver & Passenger","No data"),]

```
#####Q32 Get penultimate row of DT, Get last row of DT

```{r}

# it is c, but not list

DT[c(.N, .N-1)]

tail(DT,2)

```
#####Q33 Reorder columns of DT as V2 , V1, V3

```{r}

DT <- data.table(V1= c("a","b","c","d"), V2 =c("1","2","3","4"), V3 = c("aa","bb","cc","dd") )

DT

setcolorder(DT, c('V2','V1','V3'))

DT

```
#####Q35 Select column Manufacturer and compute standard deviation of column Price and return a single value that gets recycled

```{r}

DT <-  fread('https://raw.githubusercontent.com/selva86/datasets/master/Cars93_miss.csv')

DT

DT[,.(Sd.Price = mean(Price, na.rm = TRUE)), by = Manufacturer]

# return duplicated manufacturer
DT[,.(Manufacturer, Sd.Price = mean(Price, na.rm = TRUE))]


```
#####Q36 Calculate sum of Weight of cars under every Manufacturer but exclude all 'Midsize' Type cars

```{r}

DT[Type != 'Midsize', sum(Weight), by = Manufacturer]

```
#####Q37 Find the row position of the 5th largest value of column Price in DT.

```{r}

DT <-  fread('https://raw.githubusercontent.com/selva86/datasets/master/Cars93_miss.csv')

DT[, row_num := .I][order(-Price)][5,row_num]

```
#####Q38 Replace all values of Length column in the lower 5%ile and greater than 95%ile with respective 5th and 95th %ile value

```{r}

DT <-  fread('https://raw.githubusercontent.com/selva86/datasets/master/Cars93_miss.csv')

DT[, Length := ifelse(Length < quantile(Length, 0.05, na.rm = TRUE), quantile(Length, 0.05, na.rm = TRUE), ifelse(Length > quantile(Length, 0.95, na.rm = TRUE), quantile(Length, 0.95, na.rm = TRUE), Length))]

DT

```
#####Q39 Swap column V1 & V3 for all the rows where V2 = 2

```{r}

DT <- data.table(V1= c("a","b","c","d"), 
                 V2 =c("1","2","2","3"), 
                 V3 = c("aa","bb","cc","dd") )

DT

DT[V2==2, c('V1', 'V3') := .(V3, V1)]

DT


```
#####Q40 Swap rows 1 and 2 in DT

```{r}

DT <- data.table(V1= c("a","b","c","d"), 
                 V2 =c("1","2","2","3"), 
                 V3 = c("aa","bb","cc","dd") )
DT

rbind(DT[2,], DT[!c(2),])

```
#####Q41 Reverse all the rows of DT so that topmost row goes to bottom

```{r}

DT <- data.table(V1= c("a","b","c","d"), 
                 V2 =c("1","2","2","3"), 
                 V3 = c("aa","bb","cc","dd") )
DT

DT[,row_num := .I][order(-row_num)][,!c('row_num')]

```
#####Q42 Get one-hot encodings for columns Gender & Degree in the data.table DT and append it as columns

```{r}

DT <- data.table(Gender = c("M","M","F","F"), E_Id =c(1,2,3,4), Degree = c("UG","PG","PhD","UG") )

DT

DT1 <- melt(DT, id.vars = 'E_Id', measure.vars = c('Gender','Degree'), variable.name = 'item', value.name = 'item_value')

dcast(DT1, E_Id ~ item + item_value, fun.aggregate = length)

```
#####Q43 Find row-wise sum values of DT

```{r}

# ? should be col wise median?

DT <-  fread('https://raw.githubusercontent.com/selva86/datasets/master/Cars93_miss.csv')

sapply(DT[, which(sapply(DT, is.numeric)), with = FALSE], function(x) median(x, na.rm = TRUE))

```
#####Q44 Compute correlation score of each column against other columns

```{r}

DT <-  fread('https://raw.githubusercontent.com/selva86/datasets/master/Cars93_miss.csv')

cor(DT[,.SD, .SDcols = which(sapply(DT, is.numeric))], use = "na.or.complete")

```
#####Q45 Compute the minimum-by-maximum for every row of df

```{r}

set.seed(123)

DT <- data.table(V1 = runif(10, 1, 100), 
                 V2 = runif(10, 1, 80), 
                 V3 = runif(10, 1, 100))

DT

DT[,min_max := apply(.SD,1, min)/apply(.SD,1,max)][]

```
#####Q46 Normalize all columns in DT

```{r}

set.seed(235)

DT <- data.table(V1= runif(10, 1, 100), 
                 V2 =runif(10, 1, 80), 
                 V3 = runif(10, 1, 100))

sapply(DT,function(x) scale(x))

DT[,lapply(.SD, function(x) scale(x))]

```
#####Q47 Compute the correlation of each column of df with its succeeding column

```{r}

DT <-  fread('https://raw.githubusercontent.com/selva86/datasets/master/Cars93_miss.csv')

DT1 <-DT[, .SD, .SDcols = which(sapply(DT,is.numeric))]

for (i in 1:(ncol(DT1) - 1)){
    a <- i + 1
    print(cor(DT1[,..i], DT1[,..a], use = "na.or.complete"))
}

```
#####Q48 Replace both values in both diagonals of DT with 0

```{r}

DT <- data.table(V1= runif(4, 1, 100), V2 =runif(4, 1, 80), V3 = runif(4, 1, 100), V4 = runif(4,1, 50))

DT

for (i in 1:nrow(DT)){
    DT[i,i] <- 0
    DT[nrow(DT) - i + 1, i] <- 0
}

DT

```
#####Q49 Join dataframes df1 and df2 by ‘fruit-pazham’ and ‘weight-kilo’

```{r}

DT1 = data.table(fruit =  c('apple', 'banana', 'orange'),
                    weight =  c('high', 'medium', 'low'),
                    price=  c(1,2,3))

DT2 = data.table(pazham =  c('apple', 'orange', 'pine'),
                    kilo= c('high', 'low', 'high'),
                    price =  c(1,2,3))

merge(DT1,DT2, by.x = c('fruit','weight'), by.y = c('pazham','kilo'))

# join?
DT1[DT2, on = c('fruit' = 'pazham', 'weight' = 'kilo')]

```
#####Q50 Get the positions where values of two columns of DT match

```{r}

DT <- data.table(fruit1 =  c('apple', 'orange', 'banana', 'mango'),
                 fruit2 =  c('apple', 'grapes', 'banana', 'jackfruit'))

DT

# DT[,row_num := .I][fruit1 == fruit2,]

which(DT$fruit1==DT$fruit2)

```
#####Q51 Create two new columns in df, one of which is a lag 1 (shift column a down by 1 row) of column ‘a’ and the other is a lead 1 (shift column b up by 1 row)

```{r}

DT <- data.table(V1 = runif(4, 1, 100), 
                 V2 = runif(4, 1, 80), 
                 V3 = runif(4, 1, 100), 
                 V4 = runif(4,1, 50))

DT

DT[,c('lag1','lead1') := .(shift(V1,1), shift(V1,1, type = 'lead'))][]

```
#####Q52 Get the frequency of unique values in the entire dataframe DT
```{r}

DT <- data.table(fruit =  c('apple', 'orange', 'banana', 'apple'))

DT[,.N,by = fruit]

```
#####Q53 Create a pivot table out of DT keeping ID & Month as key  
dcast(data, formula, fun.aggregate = NULL, sep = "_",
    ..., margins = NULL, subset = NULL, fill = NULL,
    drop = TRUE, value.var = guess(data),
    verbose = getOption("datatable.verbose"))
```{r}

set.seed(569)

DT <- data.table(  
    ID       = sample(1:20, 5, replace=TRUE), 
    Month    = sample(1:12, 5, replace=TRUE),
    Category = c("Drinks", "Food", "Drinks", "Food", "Food"),
    Expenses = runif(5),
    key      = c('ID', 'Month')
)

DT

dcast(DT, ID + Month ~ Category, fun.aggregate = function(x) sum(x, na.rm = TRUE), value.var = 'Expenses')

```
#####Q54 Create a bootstrap sample of length 5 from DT
```{r}

set.seed(568)

DT <- data.table(V1 = runif(20, 1, 100), 
                 V2 = runif(20, 1, 80), 
                 V3 = runif(20, 1, 100), 
                 V4 = runif(20,1, 50))

DT

b_sample <- sample(1:20, size = 5, replace = TRUE)

DT[b_sample, ]

```
#####Q55 In DT, create a new column (cat_col, that has 'fail' if the average of the first two columns is less than 40. Else, it contains 'pass'

```{r}

set.seed(100)
DT <- data.table(V1 = runif(20, 1, 100), 
                 V2 = runif(20, 1, 80))

DT

DT[, cat_col := ifelse(apply(.SD, 1, function(x) mean(x, na.rm = TRUE))<40,'fail','pass')][]

```
#####Q56 Convert DT from wide to long format by keeping ID & Month as key

melt(data, id.vars, measure.vars,
    variable.name = "variable", value.name = "value",
    ..., na.rm = FALSE, variable.factor = TRUE,
    value.factor = FALSE,
    verbose = getOption("datatable.verbose"))

```{r}

set.seed(693)

DT <- data.table(
  ID       = sample(1:20, 5, replace=TRUE), 
  Month    = sample(1:12, 5, replace=TRUE),
  Drinks   = sample(10:20,5, replace = TRUE),
  Food     = sample(10:20,5, replace = TRUE),
  Starters = sample(5:15,5, replace = TRUE)   
)

DT

melt(DT, id.vars = c('ID','Month'), measure.vars = c('Drinks', 'Food', 'Starters'))

```
#####Q57 Append a new column in DT with first three lag of column V1

```{r}

set.seed(1369)

DT <- data.table(Date = seq(as.Date('2011-01-01'),as.Date('2011-01-10'),by = 1), 
                 V1   = round(runif(10, 1, 80),2))

DT

DT[,c('lag1','lag2','lag3') := .(shift(V1,1), shift(V1,2), shift(V1,3))][]

```
#####Q58 Shuffle rows of DT

```{r}

set.seed(869)

DT <- data.table(Date = seq(as.Date('2011-01-01'),as.Date('2011-01-10'),by = 1), 
                 V1   = runif(10, 1, 80), 
                 V2   = runif(10, 1, 100), 
                 V3   = runif(10,1, 50))

DT

DT[sample(1:nrow(DT)),]

```
#####Q59 Select all rows between dates 2011-01-05 & 2011-01-08 in DT

```{r}

set.seed(100)
DT <- data.table(Date = seq(as.Date('2011-01-01'),as.Date('2011-01-10'),by = 1), 
                 V1   = runif(10, 1, 80), 
                 V2   = runif(10, 1, 100), 
                 V3   = runif(10,1, 50))

DT[between(Date, as.Date('2011-01-05'), as.Date('2011-01-08'), incbounds = FALSE)]

```
[tips & tricks](http://brooksandrew.github.io/simpleblog/articles/advanced-data-table/)

Fast looping with set

```{r}

dt <- data.table(mtcars)

dt

for (j in c(1L,2L,4L)) set(dt, j=j, value = -dt[[j]]) # integers using 'L' passed for efficiency

dt

```





































































