---
title: "Kaggle March 2021"
author: "Helen Zhang"
date: '2021-04-24'
slug: []
categories: ["R"]
tags: ["Kaggle"]
description: "Kaggle playground March 2021"
lastmod: "`r Sys.Date()`"
thumbnail: ''
summary: "Kaggle playground March 2021"

---


```{r setup, include=FALSE}

knitr::opts_chunk$set(echo = TRUE)

```

load library

```{r include=FALSE}

library(tidyverse)
library(data.table)
library(h2o)

```


```{r}

train_df <- fread("train.csv") %>%
mutate_if(is.character, as.factor) %>%
mutate(target = as.factor(target))

test_df <- fread("test.csv") %>%
mutate_if(is.character, as.factor)

```


```{r}

h2o.init()

train_h2o <- h2o.assign(as.h2o(train_df),'train')

# test_h2o <- h2o.assign(as.h2o(test_df),'test')

target <- 'target'

predictors <- setdiff(colnames(train_df),c('id','target'))

```


```{r eval=FALSE, include=FALSE}

automl_fit <- h2o.automl(
      x = predictors,
      y = target,
      balance_classes = TRUE,
      training_frame = train_h2o,
      # leaderboard_frame = valid_h2o,
      seed = 1354,
      keep_cross_validation_predictions = TRUE,
      keep_cross_validation_models = TRUE,
      verbosity = "info",
      max_runtime_secs = 10800
)

```



```{r eval=FALSE, include=FALSE}

h2o.init()

automl_fit <- h2o.loadModel(path = "c:\\Users\\marz\\Downloads\\StackedEnsemble_AllModels_AutoML_20210521_093046")

automl_fit


```



```{r eval=FALSE, include=FALSE}

h2o.explain_row(automl_fit, as.h2o(train_df), row_index = 56125)

```



```{r eval=FALSE, include=FALSE}

h2o.shap_explain_row_plot(automl_fit, as.h2o(train_df))

```



















