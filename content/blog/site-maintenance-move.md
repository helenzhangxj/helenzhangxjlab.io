---
date: "2021-04-11"
lastmod: "`r format(Sys.time(), '%b %d, %Y')`"
linktitle: Site Maintenance Move
categories:
- Site R&R

title: Site Maintenance blogger move
---

move contents from google blogger to gitlab page

<!--more-->

ML algorithm
Linear regression
Logistic regression
Decision trees
Random forest
PCA/PCR
LDA/SVM
Xgboost/ Adaboost
Time series
KNN
Naïve bayes
K-mean
SVD
Neural network


Links:
Decision Trees in R
http://www.learnbymarketing.com/tutorials/rpart-decision-trees-in-r/

AMES Housing Price Project Solution
http://rstudio-pubs-static.s3.amazonaws.com/411830_faa32213605444738e864319dcc5d25e.html



used for both classification and regression
is capable of handling heterogeneous  as well as missing data

Heterogeneity in statistics means that your populations, samples or results are different. It is the opposite of homogeneity, which means that the population/data/results are the same.
A heterogeneous population or sample is one where every member has a different value for the characteristic you’re interested in. For example, if everyone in your group varied between 4’3″ and 7’6″ tall, they would be heterogeneous for height. In real life, heterogeneous populations are extremely common. For example, patients are typically a very heterogeneous population as they differ with many factors including demographics, diagnostic test results, and medical histories.

Link:
http://www.learnbymarketing.com/tutorials/rpart-decision-trees-in-r/
https://www.statmethods.net/advstats/cart.html
https://www.stat.berkeley.edu/~breiman/RandomForests/cc_home.htm
https://www.gormanalysis.com/blog/random-forest-from-top-to-bottom/
https://www.gormanalysis.com/blog/magic-behind-constructing-a-decision-tree/
https://data-flair.training/blogs/r-decision-trees/
https://stackoverflow.com/questions/36781755/how-to-specify-minbucket-in-caret-train-for
https://stats.stackexchange.com/questions/98953/why-doesnt-random-forest-handle-missing-values-in-predictors
https://www.jeremyjordan.me/hyperparameter-tuning/
https://dzone.com/articles/decision-trees-and-pruning-in-r
https://stats.stackexchange.com/questions/28029/training-a-decision-tree-against-unbalanced-data



http://brooksandrew.github.io/simpleblog/articles/advanced-data-table/
https://www.analyticsvidhya.com/blog/2019/12/6-powerful-feature-engineering-techniques-time-series/
https://mgimond.github.io/ES218/Week03d.html



https://cran.r-project.org/web/packages/xgboost/vignettes/discoverYourData.html
https://www.r-bloggers.com/machine-learning-basics-gradient-boosting-xgboost/
http://appliedpredictivemodeling.com/blog/2013/10/23/the-basics-of-encoding-categorical-data-for-predictive-models
https://xgboost-clone.readthedocs.io/en/latest/parameter.html#parameters-in-r-package

time series
decomposite: decomposition of a time series leads to identify and extract individual components
primary objective of decomposition is to study the components of the time series. Not forecasting, however, forecasting models can be built on top of the decomposed series
 - check the pattern

components of time series
seasonality
trend
cycle

white noise: purely random
ggAcf() <- consist of many insignificant spikes, correlations between observations tend to be 0 (i.e. no auto correlation), would expect first 10 -15 are within the range
can examine the ggAcf plot to tell if the dataset is white noise
can also run Ljung-box test

As you learned in the video, white noise is a term that describes purely random data. You can conduct a Ljung-Box test using the function below to confirm the randomness of a series; a p-value greater than 0.05 suggests that the data are not significantly different from white noise.
> Box.test(pigs, lag = 24, fitdf = 0, type = "Ljung")
There is a well-known result in economics called the "Efficient Market Hypothesis" that states that asset prices reflect all available information. A consequence of this is that the daily changes in stock prices should behave like white noise (ignoring dividends, interest rates and transaction costs). The consequence for forecasters is that the best forecast of the future price is the current price.

additive model: Yt = Tt + St + lt, when the resultant series is the sum of the componets
multiplicative model: Yt = Tt * St * lt  when resultant time series is the product of the components
a series may be considered multiplicative series when the seasonal fluctuations increases as trend increases. a multiplicative time series can be transformed into an additive series by taking log transformation
log(Yt) = log(Tt) + log(St) + log(Lt)

so the question becomes whether use the original scale or the logged

pipelines
identify components (season, trend, irregular)
  plots that identify seasonality better
  plot1 - seasonal plot (ggseasonalplot)
  plot 2 - ggseasonalplot(polar = T)
  plot3 - monthplot()

decompose the components
  tsdecompose <- decompose(ts, type = 'multiplicative')
  (to seasonal and trend, i.e. Yt = Tt * St*It (random components)
  by examine the trend and seasonality, can determine whether is additive or multica??
  plot(tsdecompose)

inspect trend
 stl() - give error bars, if the error is significant or not, only for additive model

propose model
forecast method
   white noise
   random walk (with, without drift)

regression

The first difference transformation of a time series z[t] consists of the differences (changes) between successive observations over time, that is z[t]−z[t−1].
Differencing a time series can remove a time trend. The function diff() will calculate the first difference or change series. A difference series lets you examine the increments or changes in a given time series. It always has one fewer observations than the original series.

dataset is stationary

distance from the mean (variance) is equal
Covariance is equal

http://datasideoflife.com/?p=620
https://cran.r-project.org/web/packages/forecastML/vignettes/grouped_forecast.html
https://cran.rstudio.com/web/packages/sweep/vignettes/SW02_Forecasting_Multiple_Models.html
https://www.r-bloggers.com/time-series-machine-learning-and-feature-engineering-in-r/




