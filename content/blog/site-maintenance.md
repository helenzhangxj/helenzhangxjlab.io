---
date: "2021-04-10"
lastmod: "`r format(Sys.time(), '%b %d, %Y')`"
linktitle: Site Maintenance
categories:
  - Site R&R
tags:
  - blogdown
  - hugo
  - git
  - gitlab
thumbnail: img/img_2021_04_10_f.jpg
#menu:
#  main:
#    name: Jekyll migration
#    weight: 10
title: Site Maintenance
---
I am very excited to try the R blogdown package to create a static personal blogsite.  
to add space  
<!--more-->

During the easter weekend, have spent a few hours: 
  - go through a few tutorials online
  - browse a few websites that i liked a lot for layout ideas
  - pick a hugo theme and make some modificaitons
  - write a post
  - deploy via Gitlab page

###### Resources & tools
* [notion](https://www.notion.so/Blog-site-mood-board-1f280d3b11914b6a9af8030241091e15), for mood board
* tutorials
  * [Introduction to Blogdown](https://www.youtube.com/watch?v=CjTLN-FXiFA) by RStudio
  * [blogdown: Creating Websites with R Markdown](https://bookdown.org/yihui/blogdown/)
  * [Best R Hugo Blogdown Site End to End Tutorial](https://www.youtube.com/watch?v=9Jqvaoeh1W4&t=674s)
  * [Hugo](https://www.youtube.com/watch?v=qtIqKaDlqXo&list=PLLAZ4kZ9dFpOnyRlyS-liKL5ReHDcj4G3)
  * [Deploying a Blogdown Website with GitLab Pages](https://blog.zenggyu.com/en/post/2019-02-08/deploying-a-blogdown-website-with-gitlab-pages/)
  * [How to make a website with Hugo and Gitlab](https://www.youtube.com/watch?v=-q6ZiCroiGM&t=1344s)
  * [markdown syntax](https://www.markdownguide.org/basic-syntax/)
* microsoft photo collage maker, for thumbnails 
* [icon converter](https://convertico.com/)  
* theme, layout ideas: 
    [hugo themes](https://themes.gohugo.io/)
    [hugo themes](https://hugothemesfree.com/)
    [squarespace](https://www.squarespace.com/templates)
    [How To Build A Personal Website with Hugo](https://matteocourthoud.github.io/post/website/)

###### Git commands

        git --version
        git config --global user.name"xxx"
        git config --global user.name
        
        git config --global user.email"xxx"
        git config --global user.email
        
        git config --global --list
        
        git init
        git status
        git add xxx/git add .
        git commit -m "initial commit"
        # url get under clone, http, gitlab; branch name (i.e. master)
        git push -u "url" branchname
        git pull

  * [Getting started with Git Commands](https://www.youtube.com/watch?v=OWaZXtgq28c)
  * [Gitlab commands cheatsheet](https://about.gitlab.com/images/press/git-cheat-sheet.pdf) 

###### Future changes & improvements
  - [ ] create new favicon  
  - [ ] align font & font size
  - [ ] create _comments_ partials, find a _comments_ host (commento is good, but  not free)
  - [ ] ~~create _site search_~~, check [this](https://gohugo.io/tools/search/) & [this](https://blog.humblepg.com/post/2019/06/hugo-search.html)
  - [ ] implement _site search_ via agolia, check [this](https://jimmysong.io/hugo-handbook/steps/searching-plugin.html)
  - [ ] ~~align image with text, equal height, check out this [theme](https://github.com/opera7133/Blonde)~~;  (applied _readmore_ instead of using grid layout)
  - [ ] ~~add _home_ tab in menu~~
  - [ ] ~~update _head_ file, markdown hyperlink open in new tab~~ (target = "_blank")
  - [ ] update about page, intro section
  - [ ] image quality, take photos
  - [ ] change image default size for post
  - [ ] add _related content_ for single template
  - [ ] homepage, gallery carousel with links
  - [ ] ~~homepage, image position updated via css~~ (for consistency, easier to update the image itself)
  - [ ] ~~button to _back to top_~~ (added in the footer section)
  - [ ] page views counter partial?
  - [ ] contact form page?
  - [ ] add google search & analytics
  - [ ] ~~add lastmod date~~
  - [ ] active table of content
  - [ ] ~~add_summary_in front matter~~ (html more tag broke the plot display)
